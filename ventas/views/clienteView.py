from django.views.decorators.csrf import csrf_exempt
from ventas.controllers import ClienteController

class ClienteView():
    @csrf_exempt
    def clienteAPI(request, id = 0):
        if request.method == 'GET':
            return ClienteController.consultarClientes()
        elif request.method == 'POST':
            return ClienteController.crearCliente(request)
        elif request.method == 'PUT':
            return ClienteController.modificarCliente(request)
        elif request.method == 'DELETE':
            return ClienteController.eliminarCliente(id)