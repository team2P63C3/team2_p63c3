from django.views.decorators.csrf import csrf_exempt
from ventas.controllers import VentaController

class VentaView():
    @csrf_exempt
    def ventaAPI(request, id = 0):
        if request.method == 'GET':
            return VentaController.consultarVentas()
        elif request.method == 'POST':
            return VentaController.registrarVenta(request)