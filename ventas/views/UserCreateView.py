from django.http.response import JsonResponse
from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from ventas.serializers.usuarioSerializer import UsuarioSerializer

class UserCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = UsuarioSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print(serializer.validated_data)
        serializer.save()
        tokenData = {"nombre_usuario":request.data["nombre_usuario"],
                    "password":request.data["password"]}
        print(tokenData)
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        print("llega")
        try:
            tokenSerializer.is_valid(raise_exception=True)
            print("llega2")
            print (tokenSerializer.validated_data)
            return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)
        except Exception as e:
            print(e)
            return JsonResponse("falló",safe=False)