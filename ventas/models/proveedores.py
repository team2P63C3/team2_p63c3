from django.db import models

class Proveedores(models.Model):
    prov_id = models.AutoField(primary_key=True)
    prov_nombre = models.CharField(max_length=30, null=False)
    prov_nit = models.CharField(max_length=15)
    prov_telefono = models.BigIntegerField()