from django.db import models

class Cliente(models.Model):
    cli_id = models.BigIntegerField(primary_key=True, null=False)
    cli_nombre = models.CharField(max_length=50, null=False)