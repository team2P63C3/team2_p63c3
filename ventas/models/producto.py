from django.db import models

from ventas.models.categoria import Categoria

class Producto(models.Model):
    prod_id = models.AutoField(primary_key=True)
    prod_nombre = models.CharField(max_length=50)
    prod_categoria = models.ForeignKey(Categoria, null=False, on_delete=models.CASCADE, default=None)
    prod_color = models.CharField(max_length=50)
    prod_descripcion = models.CharField(max_length=255)
    prod_precio_unitario = models.FloatField(null=False, default=0)
