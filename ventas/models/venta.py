from django.db import models

from ventas.models import Cliente, Producto

class Venta(models.Model):
    id = models.BigAutoField(primary_key=True)
    cliente = models.OneToOneField(Cliente, on_delete=models.CASCADE, null=True, default=None)
    producto = models.ManyToManyField(Producto, default=None)
    total_venta = models.FloatField(null=False, default=0)
    fecha_venta = models.DateField(auto_now=True)