from django.db import models

from ventas.models.producto import Producto

class Devoluciones(models.Model):
    dev_id = models.AutoField(primary_key=True)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    dev_cantidad = models.IntegerField(null=False,default=0)
    dev_motivo = models.CharField(max_length=150)
    cliente = models.CharField(max_length=70)
    dev_total = models.FloatField(null=False,default=0)
    dev_fecha = models.DateField(auto_now=True) 