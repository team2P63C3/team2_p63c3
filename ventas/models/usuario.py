from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password
from django.db.models.fields import BooleanField

class ManejadorUsuario(BaseUserManager):
    def create_user(self, nombre_usuario, password=None):
        if not nombre_usuario:
            raise ValueError('El usuario debe tener nombre de usuario')
        usuario = self.model(nombre_usuario=nombre_usuario)
        usuario.set_password(password)
        usuario.is_staff = True
        usuario.save(using=self._db)
        return usuario

    def create_superuser(self, nombre_usuario, password):
        usuario = self.create_user(
            nombre_usuario=nombre_usuario,
            password=password
        )
        usuario.is_admin = True
        usuario.save(using=self._db)
        return usuario

class Usuario(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    nombre_usuario = models.CharField('Usuario', max_length=15, unique=True)
    password = models.CharField('Password', max_length=256)
    nombre = models.CharField('Nombre', max_length=30)
    email = models.EmailField('Email', max_length=100)
    is_staff = BooleanField(default=False)
    
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = ManejadorUsuario()
    USERNAME_FIELD = 'nombre_usuario'