from django.contrib import admin
from .models.usuario import Usuario
from .models.account import Account
admin.site.register(Usuario)
admin.site.register(Account)
# Register your models here.