from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from ventas.models.categoria import Categoria
from ventas.serializers.categoriaSerializer import CategoriaSerializer

@csrf_exempt
def CategoriaAPI(request, id = 0):
    if request.method == 'GET':
        if id != 0:
            categoria_unitaria = Categoria.objects.get(cat_id = id)
            categoria_serializer = CategoriaSerializer(categoria_unitaria, many=False)
            return JsonResponse(categoria_serializer.data, safe=False)
        else:
            categorias = Categoria.objects.all()
            categorias_serializer = CategoriaSerializer(categorias, many=True)
            return JsonResponse(categorias_serializer.data, safe=False)
    if request.method == 'POST':
        categoria_data = JSONParser().parse(request)
        categoria_serializer = CategoriaSerializer(data=categoria_data)
        if categoria_serializer.is_valid():
            categoria_serializer.save()
            return JsonResponse("Añadido correctamente!", safe=False)
        return JsonResponse("Hubo un error al intentar añadir la categoria.", safe=False)
    if request.method == 'PUT':
        categoria_data = JSONParser().parse(request)
        categoria = Categoria.objects.get(cat_id = categoria_data['cat_id'])
        categoria_serializer = CategoriaSerializer(categoria, data=categoria_data)
        if categoria_serializer.is_valid():
            categoria_serializer.save()
            return JsonResponse("Registro modificado correctamente!", safe=False)
        return JsonResponse("Hubo un error al modificar el registro de categoria.", safe=False)
    if request.method == 'DELETE':
        categoria = Categoria.objects.get(cat_id = id)
        categoria.delete()
        return JsonResponse("Categoria eliminada correctamente!", safe=False)