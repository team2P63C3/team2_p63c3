from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from ventas.models import Proveedores
from ventas.serializers.proveedoresSerializer import ProveedoresSerializer

@csrf_exempt
def ProveedoresAPI(request, id = 0):
    if request.method == 'GET':
        proveedores = Proveedores.objects.all()
        proveedores_serializer = ProveedoresSerializer(proveedores, many = True)
        return JsonResponse(proveedores_serializer.data, safe=False)
    if request.method == 'POST':
        proveedor_data = JSONParser().parse(request)
        try:
            proveedor_serializer = ProveedoresSerializer(data=proveedor_data)
            if proveedor_serializer.is_valid():
                proveedor_serializer.save()
                return JsonResponse("Proveedor agregado correctamente!", safe=False)
            return JsonResponse("Los datos ingresados no son válidos.", safe=False)
        except:
            return JsonResponse("No se pudo agregar al proveedor.", safe=False)
    if request.method == 'PUT':
        proveedor_datos = JSONParser().parse(request)
        proveedor_a_actualizar = Proveedores.objects.get(prov_id=proveedor_datos["prov_id"])
        proveedor_serializer = ProveedoresSerializer(proveedor_a_actualizar, data=proveedor_datos)
        if proveedor_serializer.is_valid():
            proveedor_serializer.save()
            return JsonResponse("Se modifico correctamente el proveedor", safe=False)
        return JsonResponse("Fallo al modificar el proveedor", safe=False)
    if request.method == 'DELETE':
        proveedor_a_eliminar = Proveedores.objects.get(prov_id=id)
        proveedor_a_eliminar.delete()
        return JsonResponse("Se ha eliminado correctamente el proveedor")