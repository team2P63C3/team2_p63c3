from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from ventas.models import Inventario, Producto, Categoria
from ventas.serializers.inventarioSerializer import InventarioSerializer

@csrf_exempt
def InventarioAPI(request, id = 0):
    if request.method == 'GET':
        lista_elementos = Inventario.objects.all()
        lista_elementos_serializer = InventarioSerializer(lista_elementos, many=True)
        return JsonResponse(lista_elementos_serializer.data, safe=False)

    if request.method == 'POST':
        datos = JSONParser().parse(request)
        try:
            producto = Producto.objects.get(prod_id = datos["producto"]["prod_id"])
            try:
                Inventario.objects.get_or_create(producto = producto, cantidad = datos["cantidad"])
                return JsonResponse("Hecho", safe=False)
            except BaseException as err:
                print(err)
                return JsonResponse("Error en detalle", safe=False)
        except:
            return JsonResponse("No encontró el producto!", safe=False)