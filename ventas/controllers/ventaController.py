from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from ventas.models import Venta, Cliente, Producto
from ventas.serializers import ClienteSerializer

class VentaController():
    def registrarVenta(venta):
        venta_datos = JSONParser().parse(venta)
        try:
            comprador = Cliente.objects.get(cli_id=venta_datos["cliente"])
            try:
                venta_nueva = Venta.objects.create(
                    cliente=comprador,
                    total_venta=venta_datos["total_venta"]
                )
                try:
                    for p in venta_datos["producto"]:
                        producto = Producto.objects.get(prod_id = p)
                        venta_nueva.producto.add(producto)
                    try:
                        venta_nueva.save()
                        return JsonResponse("Venta regitrada correctamente!", safe=False)
                    except:
                        return JsonResponse("Error al guardar la venta", safe=False)
                except:
                    return JsonResponse("Error al registrar productos a la venta", safe=False)                    
            except:
                return JsonResponse("Hubo un error al tratar de crear la venta", safe=False)
        except:
            return JsonResponse("No se encontró el cliente", safe=False)
    
    def consultarVentas():
        ventas = Venta.objects.all()
        lista_ventas = []
        for venta in ventas:
            cliente = ClienteSerializer(venta.cliente).data
            producto = []
            for prod in venta.producto.values():
                producto.append(prod)
            lista_ventas.append({
                "id": venta.id,
                "cliente": cliente,
                "producto": producto,
                "total_venta": venta.total_venta
            })
        return JsonResponse(lista_ventas, safe=False)