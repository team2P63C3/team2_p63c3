from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from ventas.models import Devoluciones
from ventas.serializers.devolucionesSerializer import DevolucionesSerializer

@csrf_exempt
def DevolucionesAPI(request, id = 0):
    if request.method == 'GET':
        devolucion = Devoluciones.objects.all()
        devolucion_serializer = DevolucionesSerializer(devolucion, many=True)
        return JsonResponse(devolucion_serializer.data, safe=False)
    if request.method == 'POST':
        devoluciones_data = JSONParser().parse(request)
        devoluciones_serializer = DevolucionesSerializer(data=devoluciones_data)
        if devoluciones_serializer.is_valid():
            devoluciones_serializer.save()
            return JsonResponse("El producto ha sido devuelto correctamente!", safe=False)
        return JsonResponse("Hubo un error al intentar añadir el producto.", safe=False)
    if request.method == 'PUT':
        devoluciones_data = JSONParser().parse(request)
        devoluciones = Devoluciones.objects.get(dev_id = devoluciones_data['producto'])
        devoluciones_serializer = DevolucionesSerializer(devoluciones, data=devoluciones_data)
        if devoluciones_serializer.is_valid():
            devoluciones_serializer.save()
            return JsonResponse("Registro modificado exitosamente!", safe=False)
        return JsonResponse("Hubo un error al modificar el registro de devoluciones.", safe=False)
    if request.method == 'DELETE':
        devoluciones = Devoluciones.objects.get(producto = id)
        devoluciones.delete()
        return JsonResponse("Devolucion eliminada correctamente!", safe=False)