from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from ventas.models import Cliente
from ventas.serializers import ClienteSerializer

class ClienteController():
    def crearCliente(cliente):
        cliente_datos = JSONParser().parse(cliente)
        cliente_serializado = ClienteSerializer(data=cliente_datos)
        if cliente_serializado.is_valid():
            cliente_serializado.save()
            return JsonResponse(cliente_serializado.data, safe=False)
        return JsonResponse("Error al crear el cliente!", safe=False)
    
    def consultarClientes():
        clientes = Cliente.objects.all()
        clientes_lista = ClienteSerializer(clientes, many = True)
        return JsonResponse(clientes_lista.data, safe=False)

    def modificarCliente(cliente):
        cliente_datos = JSONParser().parse(cliente)
        cliente_a_modificar = Cliente.objects.get(cli_id = cliente_datos["cli_id"])
        cliente_serializado = ClienteSerializer(cliente_a_modificar, data=cliente_datos)
        if cliente_serializado.is_valid():
            cliente_serializado.save()
            return JsonResponse(cliente_serializado.data, safe=False)
        return JsonResponse("Error al tratar de modificar el cliente", safe=False)
    
    def eliminarCliente(id):
        try:
            cliente_a_eliminar = Cliente.objects.get(cli_id = id)
            cliente_a_eliminar.delete()
            return JsonResponse("Cliente eliminado correctamente!", safe=False)
        except:
            return JsonResponse("El cliente no existe en la base de datos", safe=False)