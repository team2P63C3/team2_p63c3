from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from ventas.models.categoria import Categoria
from ventas.serializers.categoriaSerializer import CategoriaSerializer

from ventas.models.producto import Producto
from ventas.serializers.productoSerializer import ProductoSerializer

@csrf_exempt
def ProductoAPI(request, id = 0):
    if request.method == 'GET':
        productos = Producto.objects.all()
        productos_serializer = ProductoSerializer(productos, many=True)
        listaProductos = []
        for producto in productos_serializer.data:
            categoria = Categoria.objects.get(cat_id=producto["prod_categoria"])
            listaProductos.append({
                "prod_id": producto["prod_id"],
                "prod_nombre": producto["prod_nombre"],
                "prod_categoria": CategoriaSerializer(categoria, many=False).data,
                "prod_color": producto["prod_color"],
                "prod_descripcion": producto["prod_descripcion"],
                'prod_precio_unitario':producto["prod_precio_unitario"]
            })
        return JsonResponse(listaProductos, safe=False)
    if request.method == 'POST':
        producto = JSONParser().parse(request)
        try:
            categoria = Categoria.objects.get(cat_id=producto["prod_categoria"]["cat_id"])
            try:
                Producto.objects.get_or_create(
                    prod_nombre=producto["prod_nombre"],
                    prod_categoria=categoria,
                    prod_color=producto["prod_color"],
                    prod_descripcion=producto["prod_descripcion"],
                    prod_precio_unitario=producto["prod_precio_unitario"]
                    )
                return JsonResponse("Producto creado satisfactoriamente!", safe=False)
            except:
                return JsonResponse("El producto ya existe! no se aceptan registros duplicados", safe=False)
        except:
            return JsonResponse("La categoria no existe!", safe=False)
    if request.method == 'PUT':
        producto = JSONParser().parse(request)
        try:
            categoria = Categoria.objects.get(cat_id=producto["prod_categoria"]["cat_id"])
            try:
                producto_a_modificar = Producto.objects.get(prod_id = producto["prod_id"])
                try:
                    producto_a_modificar.prod_nombre = producto["prod_nombre"]
                    producto_a_modificar.prod_categoria = categoria
                    producto_a_modificar.prod_color = producto["prod_color"]
                    producto_a_modificar.prod_descripcion = producto["prod_descripcion"]
                    producto_a_modificar.prod_precio_unitario = producto["prod_precio_unitario"]
                    producto_a_modificar.save()
                    return JsonResponse("Producto modificado correctamente", safe=False)
                except:
                    return JsonResponse("No se pudo modificar el producto.", safe=False)
            except:
                return JsonResponse("El producto no existe!", safe=False)
        except:
            return JsonResponse("La categoria no existe!", safe=False)
    if request.method == 'DELETE':
        producto = Producto.objects.get(prod_id = id)
        producto.delete()
        return JsonResponse("Producto eliminado correctamente!", safe=False)