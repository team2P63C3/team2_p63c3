from rest_framework import serializers
from ventas.models.devoluciones import Devoluciones

class DevolucionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devoluciones
        fields = (
            'dev_id',
            'producto',
            'dev_cantidad',
            'dev_motivo',
            'cliente'
            'dev_total'
            'dev_fecha'
        )