from rest_framework import serializers

from ventas.models import Cliente

class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = (
            'cli_id',
            'cli_nombre'
        )