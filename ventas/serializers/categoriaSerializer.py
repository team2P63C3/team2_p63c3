from rest_framework import serializers
from ventas.models.categoria import Categoria

class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = (
            'cat_id',
            'cat_nombre'
            )