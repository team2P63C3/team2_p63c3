from rest_framework import serializers

from ventas.models.inventario import Inventario

class InventarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventario
        fields = (
            'producto',
            'cantidad'
        )