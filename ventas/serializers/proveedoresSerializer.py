from rest_framework import serializers

from ventas.models import Proveedores

class ProveedoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proveedores
        fields = (
            'prov_id',
            'prov_nombre',
            'prov_nit',
            'prov_telefono'
        )