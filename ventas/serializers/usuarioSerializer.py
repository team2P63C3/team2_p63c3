from rest_framework import serializers
from ventas.models.usuario import Usuario
from ventas.models.account import Account
from ventas.serializers.accountSerializer import AccountSerializer

class UsuarioSerializer(serializers.ModelSerializer):
    account = AccountSerializer()
    class Meta:
        model = Usuario
        fields = [
            'id',
            'nombre_usuario',
            'password',
            'nombre',
            'email',
            'account'
        ]

    def create(self, validated_data):
        accountData = validated_data.pop('account')
        instancia_usuario = Usuario.objects.create(**validated_data)
        Account.objects.create(user=instancia_usuario, **accountData)
        return instancia_usuario

    def to_representation(self, obj):
        usuario = Usuario.objects.get(id=obj.id)
        account = Account.objects.get(user=obj.id)
        return {
            'id': usuario.id,
            'nombre_usuario': usuario.nombre_usuario,
            #'password':usuario.password,
            'nombre': usuario.nombre, 
            'email': usuario.email,
            'account': {
                'id': account.id,
                'isActive': account.isActive
            }
        }
