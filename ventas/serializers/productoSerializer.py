from rest_framework import serializers
from ventas.models.producto import Producto

class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = (
            'prod_id',
            'prod_nombre',
            'prod_categoria',
            'prod_color',
            'prod_descripcion',
            'prod_precio_unitario'
        )