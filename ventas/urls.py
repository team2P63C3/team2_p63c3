from django.conf.urls import url
from ventas.controllers import categoriaController, productoController, inventarioController, proveedoresController, devolucionesController

from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from ventas import views

# Se establece la ruta a consultar desde el front
urlpatterns = [
    url(r'^categoria$', categoriaController.CategoriaAPI),
    url(r'^categoria/([0-9]+)$', categoriaController.CategoriaAPI),
    url(r'^producto$', productoController.ProductoAPI),
    url(r'^producto/([0-9]+)$', productoController.ProductoAPI),
    url(r'^inventario$', inventarioController.InventarioAPI),
    url(r'^proveedores$', proveedoresController.ProveedoresAPI),
    url(r'^proveedores/([0-9]+)$', proveedoresController.ProveedoresAPI),
    url(r'^devoluciones$', devolucionesController.DevolucionesAPI),
    url(r'^clientes$', views.ClienteView.clienteAPI),
    url(r'^clientes/([0-9]+)$', views.ClienteView.clienteAPI),
    url(r'^ventas$', views.VentaView.ventaAPI),

    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
        
]