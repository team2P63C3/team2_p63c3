Misión Tic Ciclo 3 - Sprint #1

Duración:
8 días (inicio: 16 de septiembre, finalización: 23 de septiembre).


Objetivo:
Organizar el equipo de desarrollo.


Actividades:

Conformar equipos de desarrollo de 5 integrantes y nombrar a un Scrum Master (SM) por equipo.
Cada equipo deberá:
Crear un sitio en Jira.
Añadir a todos los integrantes del equipo, al sitio creado.
Crear un proyecto en Jira.
Utilizar el template de Scrum.
Crear elementos de ejemplo en el Backlog.
Crear el Sprint #1.
Utilizar el tablero.
Crear un repositorio de prueba y asociarlo al proyecto creado en Jira.
Utilizar el repositorio.

Integrantes del equipo : 

Melissa Lopez Diaz, 
Luis Ángel Herrera Mantilla,
Brayan Mendez,
Jose Argumedo,
William Serrato Gutiérrez