import { createRouter, createWebHashHistory } from 'vue-router'
import App from '../App.vue'

import LogIn from '../views/LogInView.vue'
import SignUp from '../views/SignUpView.vue'
import Home from '../views/Home.vue'
import Account from '../views/AccountView.vue'
import CrearCategoria from '../views/CrearCategoriaView.vue'
import VerCategorias from '../views/VerCategoriasView.vue'
import ModificarCategoria from '../views/ModificarCategoriaView.vue'

const routes = [
  {
    path: '/',
    name: 'App',
    component: App
  },
  {
    path: '/user/logIn',
    name: 'logIn',
    component: LogIn
  },
  {
    path: '/user/signUp',
    name: 'signUp',
    component: SignUp
  },
  {
    path: '/user/home',
    name: 'home',
    component: Home
  },
  {
    path: '/user/account',
    name: 'account',
    component: Account
  },
  {
    path: '/user/categoria',
    name: 'categoria',
    component: CrearCategoria
  },
  {
    path: '/user/categorias',
    name: 'categorias',
    component: VerCategorias
  },
  {
    path: '/user/categorias/:id',
    name: 'ModificarCategoria',
    props: true,
    component: ModificarCategoria
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
